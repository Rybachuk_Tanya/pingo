
$(document).ready(function () {




    $('.location_slider').slick({
        centerMode: true,
        centerPadding: '70px',
        slidesToShow: 4,
        autoplay: true,
        dots: true,
        autoplaySpeed: 1800,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
});


$('.video_main').parent().click(function () {
    if($(this).children('.video_main').get(0).paused)
    {
        $(this).children('.video_main').get(0).play();
        $(this).children('.playpause').fadeOut();
    }else{
        $(this).children('.video_main').get(0).pause();
        $(this).children('.playpause').fadeIn();
    }
});

    // $(function(){
    //     $('div.blocks div').inview({
    //         'viewFactor': 0.3
    //     });
    // });


    $(window).on("scroll", function () {
        if ($(document).scrollTop() > 50) {
            if ($(window).width() > 1200)
            {
                $('.header_inside').addClass('js-header');
            }
        }
        if ($(document).scrollTop() < 150) {
            $('.header_inside').removeClass('js-header');
        }
    });

    $(document).on ('click', '.btn-header', function(e){
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $(document).on ('click', '.header__link', function(e){
        event.preventDefault();
        var id = $(this).attr('href');

        var top = $(id).offset().top - 100;
        console.log(id);
        console.log(top + (window.pageYOffset || document.documentElement.scrollTop));

        $('body,html').animate({scrollTop: top + (window.pageYOffset || document.documentElement.scrollTop)}, 1000);


    });


});








