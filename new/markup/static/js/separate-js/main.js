var docWidth, docHeight, docScroll;
$(window).resize(function () {
    resize()
});
$(document).ready(function () {
    resize();
});
window.onload = function () {
    resize();
}
function resize() {
    docWidth = $(window).width();
    docHeight = $(window).height();
    docScroll = $(window).scrollTop();
    canvas();
}

$(document).scroll(function () {
    docScroll = $(window).scrollTop();
    if (docScroll > 50) {
        $('.header').addClass('no-top');
    }
    else {
        $('.header').removeClass('no-top');
    }
    var _href;
    var _active = $('.paginator .active a').attr('href');
    $('section').each(function () {
        if (docScroll + docHeight / 2 > $(this).offset().top) {
            _href = '#' + $(this).attr('id');
        }
    });
    if (_active != _href) {
        $('.paginator li').removeClass('active');
        $('.paginator a[href$="' + _href + '"]').parent().addClass('active');
    }
});
$(document).on('click', '.scroll-to', function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $('html,body').animate({scrollTop: $(href).offset().top - 50})
});


var forEach = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
};
var randomIntFromInterval = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

var $mapPins = document.querySelectorAll('#Map-shape g');

// Setup timelines attached to each map pin
forEach($mapPins, function (index, value) {
    // Group opacity timeline
    value.groupTimeline = new TimelineMax({
        paused: true
    });

    value.groupTimeline
        .to(value, 0.25, {
            opacity: 0
        });

    // Pulse animation
    var pinTimeline = new TimelineMax({
        repeat: -1,
        delay: randomIntFromInterval(1, 3),
        repeatDelay: randomIntFromInterval(0, 1)
    });

    pinTimeline.to(value.querySelector('.Pin-back'), 3, {
        scale: 50,
        transformOrigin: 'center center',
        opacity: 0
    });
});

forEach(document.querySelectorAll('.js-Location-nav [data-location]'), function (index, value) {

    value.addEventListener('mouseenter', function (e) {
        var location = e.target.getAttribute('data-location');

        // Hide other map pins
        forEach($mapPins, function (index, value) {
            if (value.getAttribute('data-location') !== location) {
                value.groupTimeline.play();
            }
        });
    }, false);

    value.addEventListener('mouseleave', function (e) {
        // Reverse all hidden map pins
        forEach($mapPins, function (index, value) {
            value.groupTimeline.reverse();
        });

    }, false);
});

function canvas() {
    var canvas = document.getElementById('canvas');
    canvas.width = docWidth;
    canvas.height = docHeight;
    ctx = canvas.getContext('2d');
    ctx.globalAlpha = .55;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.globalCompositeOperation = 'destination-out';
    var pic = new Image();
    pic.src = 'text.png';
    pic.onload = function () {
        ctx.drawImage(pic, canvas.width / 2 - 490, canvas.height / 2 - pic.height / 2);
        ctx.globalCompositeOperation = 'source-over';
        ctx.globalAlpha = .1;
        ctx.drawImage(pic, canvas.width / 2 - 490, canvas.height / 2 - pic.height / 2);
    };
}


$('.anchorLink').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});