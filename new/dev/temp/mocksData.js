/* Module data structure */

// moduleName: {
//     dataType: {
//         property: value
//     }
// }

/* Module data example */

_template: {
    big: {
        title: 'Hello world',
            age
    :
        10,
            button
    :
        false
    }
},

'head': {
    defaults: {
        title: 'default title',
            useSocialMetaTags
    :
        true
    }
},

'mob_center': [
        {
            title: 'Create your own routes',
            after_title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed',
            img: '1.jpg'

        },
        {
            title: 'See the sights on the<br> map',
            after_title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed ',
            img: '2.jpg'

        },
        {
            title: 'Become a TRAVEL expert<br> for your friends',
            after_title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed ',
            img: '3.jpg'

        }
    ]
,

'mobile': [
        {
            title: 'Set the city and start browsing trough various tours ',
            number : '1'
        },
        {
            title: 'Discover unique palces that many tourists and guides don’t even know exist',
            number : '2'

        },
        {
            title: 'Pick date and time of your visit and purchase a tour',
            number : '3'

        }
    ]
,

__pages: [{
                name: 'index',
                href: 'index.html'
             }]